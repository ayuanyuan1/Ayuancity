package com.yuan.ayuancity.mapper;

import com.yuan.ayuancity.entity.Comment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author asuyuan
 * @since 2023-04-27
 */
public interface CommentMapper extends BaseMapper<Comment> {

    //添加评论
    int saveComment(Comment comment);

    //查询评论
    List<Comment>  findListCommentByParent(Long infoId,Long parentId);



}
