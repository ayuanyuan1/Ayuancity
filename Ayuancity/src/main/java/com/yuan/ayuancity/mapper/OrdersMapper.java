package com.yuan.ayuancity.mapper;

import com.yuan.ayuancity.entity.Orders;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author asuyuan
 * @since 2023-04-25
 */
public interface OrdersMapper extends BaseMapper<Orders> {
    //根据用户id查询订单表
    List<Orders> selectOrdersListbyUserId(Integer userId);
}
