package com.yuan.ayuancity.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yuan.ayuancity.entity.Infos;
import com.yuan.ayuancity.entity.TypeAndinfo;

import java.util.List;

/**
 * 信息连接Mapper接口
 * 
 * @author suyuan
 * @date 2023-02-27
 */
public interface TypeAndinfoMapper  extends BaseMapper<TypeAndinfo>
{
    /**
     * 查询信息连接
     * 
     * @param typeInfoId 信息连接主键
     * @return 信息连接
     */
    public TypeAndinfo selectTypeAndinfoByTypeInfoId(Long typeInfoId);

    /**
     * 查询信息连接列表
     * 
     * @param typeAndinfo 信息连接
     * @return 信息连接集合
     */
    public List<TypeAndinfo> selectTypeAndinfoList(TypeAndinfo typeAndinfo);

    /**
     * 新增信息连接
     * 
     * @param typeAndinfo 信息连接
     * @return 结果
     */
    public int insertTypeAndinfo(TypeAndinfo typeAndinfo);

    /**
     * 修改信息连接
     * 
     * @param typeAndinfo 信息连接
     * @return 结果
     */
    public int updateTypeAndinfo(TypeAndinfo typeAndinfo);

    /**
     * 删除信息连接
     * 
     * @param typeInfoId 信息连接主键
     * @return 结果
     */
    public int deleteTypeAndinfoByTypeInfoId(Long typeInfoId);

    /**
     * 批量删除信息连接
     * 
     * @param typeInfoIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTypeAndinfoByTypeInfoIds(Long[] typeInfoIds);
}
