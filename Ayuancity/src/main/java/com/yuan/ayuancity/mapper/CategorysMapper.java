package com.yuan.ayuancity.mapper;

import com.github.pagehelper.PageInfo;
import com.yuan.ayuancity.entity.Carts;
import com.yuan.ayuancity.entity.Categorys;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yuan.ayuancity.entity.Users;
import com.yuan.ayuancity.entity.dto.CategorysDto;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author asuyuan
 * @since 2023-02-07
 */
public interface CategorysMapper extends BaseMapper<Categorys> {

    /**
     * 查询供求信息类型
     *
     * @param categoryId 供求信息类型主键
     * @return 供求信息类型
     */
    public Categorys selectCategorysByCategoryId(Long categoryId);

    /**
     * 查询供求信息类型列表
     *
     * @param categorys 供求信息类型
     * @return 供求信息类型集合
     */
    public List<Categorys> selectCategorysList(Categorys categorys);

    /**
     * 新增供求信息类型
     *
     * @param categorys 供求信息类型
     * @return 结果
     */
    public int insertCategorys(Categorys categorys);

    /**
     * 修改供求信息类型
     *
     * @param categorys 供求信息类型
     * @return 结果
     */
    public int updateCategorys(Categorys categorys);

    /**
     * 删除供求信息类型
     *
     * @param categoryId 供求信息类型主键
     * @return 结果
     */
    public int deleteCategorysByCategoryId(Long categoryId);

    /**
     * 批量删除供求信息类型
     *
     * @param categoryIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCategorysByCategoryIds(Long[] categoryIds);
    /*
          页面导航栏
         */
    List<CategorysDto> getcategoryAndtype();


}

