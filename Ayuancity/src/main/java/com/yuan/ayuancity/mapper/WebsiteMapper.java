package com.yuan.ayuancity.mapper;

import com.yuan.ayuancity.entity.Website;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author asuyuan
 * @since 2023-02-07
 */
public interface WebsiteMapper extends BaseMapper<Website> {
    /**
     * 查询网站简介
     *
     * @param introduceId 网站简介主键
     * @return 网站简介
     */
    public Website selectWebsiteByIntroduceId(Long introduceId);

    /**
     * 查询网站简介列表
     *
     * @param website 网站简介
     * @return 网站简介集合
     */
    public List<Website> selectWebsiteList(Website website);

    /**
     * 新增网站简介
     *
     * @param website 网站简介
     * @return 结果
     */
    public int insertWebsite(Website website);

    /**
     * 修改网站简介
     *
     * @param website 网站简介
     * @return 结果
     */
    public int updateWebsite(Website website);

    /**
     * 删除网站简介
     *
     * @param introduceId 网站简介主键
     * @return 结果
     */
    public int deleteWebsiteByIntroduceId(Long introduceId);

    /**
     * 批量删除网站简介
     *
     * @param introduceIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWebsiteByIntroduceIds(Long[] introduceIds);
}

