package com.yuan.ayuancity.mapper;

import com.yuan.ayuancity.entity.Infos;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yuan.ayuancity.entity.dto.InfosDto;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author asuyuan
 * @since 2023-02-07
 */
public interface InfosMapper extends BaseMapper<Infos> {

    /**
     * 查询供求信息
     *
     * @param infoId 供求信息主键
     * @return 供求信息
     */
    public Infos selectInfosByInfoId(Long infoId);

    /**
     * 查询供求信息列表
     *
     * @param infos 供求信息
     * @return 供求信息集合
     */
    public List<Infos> selectInfosList(Infos infos);

    /**
     * 新增供求信息
     *
     * @param infos 供求信息
     * @return 结果
     */
    public int insertInfos(Infos infos);

    /**
     * 修改供求信息
     *
     * @param infos 供求信息
     * @return 结果
     */
    public int updateInfos(Infos infos);

    /**
     * 删除供求信息
     *
     * @param infoId 供求信息主键
     * @return 结果
     */
    public int deleteInfosByInfoId(Long infoId);

    /**
     * 批量删除供求信息
     *
     * @param infoIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteInfosByInfoIds(Long[] infoIds);

    /*
     *审核信息
     */
    List<InfosDto> verifyInfos(Infos infos);
    /*
      前端获取热门推荐
    */
    List<Infos> getHotInfos(Infos infos);
}

