package com.yuan.ayuancity.mapper;

import com.yuan.ayuancity.entity.Admins;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author asuyuan
 * @since 2023-02-07
 */
@Mapper
public interface AdminsMapper extends BaseMapper<Admins> {
    /**
     * 查询管理员
     *
     * @param adminId 管理员主键
     * @return 管理员
     */
    public Admins selectAdminsByAdminId(Long adminId);

    /**
     * 查询管理员列表
     *
     * @param Admins 管理员
     * @return 管理员集合
     */
    public List<Admins> selectAdminsList(Admins Admins);

    /**
     * 新增管理员
     *
     * @param Admins 管理员
     * @return 结果
     */
    public int insertAdmins(Admins Admins);

    /**
     * 修改管理员
     *
     * @param Admins 管理员
     * @return 结果
     */
    public int updateAdmins(Admins Admins);

    /**
     * 删除管理员
     *
     * @param adminId 管理员主键
     * @return 结果
     */
    public int deleteAdminsByAdminId(Long adminId);

    /**
     * 批量删除管理员
     *
     * @param adminIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAdminsByAdminIds(Long[] adminIds);
}
