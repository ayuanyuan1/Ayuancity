package com.yuan.ayuancity.mapper;

import com.yuan.ayuancity.entity.Types;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author asuyuan
 * @since 2023-02-07
 */
public interface TypesMapper extends BaseMapper<Types> {
    /**
     * 查询供求信息二级种类
     *
     * @param typeId 供求信息二级种类主键
     * @return 供求信息二级种类
     */
    public Types selectTypesByTypeId(Long typeId);

    /**
     * 查询供求信息二级种类列表
     *
     * @param types 供求信息二级种类
     * @return 供求信息二级种类集合
     */
    public List<Types> selectTypesList(Types types);
    public List<Types> selectTypesList2(Types types);


    /**
     * 新增供求信息二级种类
     *
     * @param types 供求信息二级种类
     * @return 结果
     */
    public int insertTypes(Types types);

    /**
     * 修改供求信息二级种类
     *
     * @param types 供求信息二级种类
     * @return 结果
     */
    public int updateTypes(Types types);

    /**
     * 删除供求信息二级种类
     *
     * @param typeId 供求信息二级种类主键
     * @return 结果
     */
    public int deleteTypesByTypeId(Long typeId);

    /**
     * 批量删除供求信息二级种类
     *
     * @param typeIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTypesByTypeIds(Long[] typeIds);
}
