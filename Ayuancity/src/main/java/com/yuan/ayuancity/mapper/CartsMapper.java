package com.yuan.ayuancity.mapper;

import com.yuan.ayuancity.entity.Carts;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yuan.ayuancity.entity.Users;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author asuyuan
 * @since 2023-04-25
 */
public interface CartsMapper extends BaseMapper<Carts> {

    //根据用户id查询购物列表
    List<Carts> selectCartsListbyUserId(Integer userId);
    //添加购物车
    int addCart(Integer userId, Integer infosId,Integer typeId);
}
