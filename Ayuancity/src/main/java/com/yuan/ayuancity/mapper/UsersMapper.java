package com.yuan.ayuancity.mapper;

import com.yuan.ayuancity.entity.Users;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author asuyuan
 * @since 2023-02-07
 */
@Mapper
public interface UsersMapper extends BaseMapper<Users> {
    /**
     * 查询用户
     *
     * @param userId 用户主键
     * @return 用户
     */
    public Users selectUsersByUserId(Long userId);

    /**
     * 查询用户列表
     *
     * @param Users 用户
     * @return 用户集合
     */
    public List<Users> selectUsersList(Users Users);

    /**
     * 新增用户
     *
     * @param Users 用户
     * @return 结果
     */
    public int insertUsers(Users Users);

    /**
     * 修改用户
     *
     * @param Users 用户
     * @return 结果
     */
    public int updateUsers(Users Users);

    /**
     * 删除用户
     *
     * @param userId 用户主键
     * @return 结果
     */
    public int deleteUsersByUserId(Long userId);

    /**
     * 批量删除用户
     *
     * @param userIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteUsersByUserIds(Long[] userIds);
}
