package com.yuan.ayuancity.service;

import com.github.pagehelper.PageInfo;
import com.yuan.ayuancity.entity.Categorys;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yuan.ayuancity.entity.dto.CategorysDto;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author asuyuan
 * @since 2023-02-07
 */
public interface CategorysService extends IService<Categorys> {

    /**
     * 查询供求信息类型
     *
     * @param categoryId 供求信息类型主键
     * @return 供求信息类型
     */
    public Categorys selectCategorysByCategoryId(Long categoryId);

    /**
     * 查询供求信息类型列表
     *
     * @param categorys 供求信息类型
     * @return 供求信息类型集合
     */
    public PageInfo<Categorys> selectCategorysList(int page, int size, Categorys categorys);

    /**
     * 新增供求信息类型
     *
     * @param categorys 供求信息类型
     * @return 结果
     */
    public int insertCategorys(Categorys categorys);

    /**
     * 修改供求信息类型
     *
     * @param categorys 供求信息类型
     * @return 结果
     */
    public int updateCategorys(Categorys categorys);

    /**
     * 批量删除供求信息类型
     *
     * @param categoryIds 需要删除的供求信息类型主键集合
     * @return 结果
     */
    public int deleteCategorysByCategoryIds(Long[] categoryIds);

    /**
     * 删除供求信息类型信息
     *
     * @param categoryId 供求信息类型主键
     * @return 结果
     */
    public int deleteCategorysByCategoryId(Long categoryId);
    /*
    页面导航栏
   */
    List<CategorysDto> getcategoryAndtype();
}
