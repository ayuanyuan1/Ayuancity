package com.yuan.ayuancity.service.impl;

import com.yuan.ayuancity.entity.Comment;
import com.yuan.ayuancity.mapper.CommentMapper;
import com.yuan.ayuancity.service.CommentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author asuyuan
 * @since 2023-04-27
 */
@Service
public class CommentServiceImpl extends ServiceImpl<CommentMapper, Comment> implements CommentService {

    @Resource
    private CommentMapper commentMapper;
    //存放迭代找出的所有子代的集合
    private List<Comment> tempReplys = new ArrayList<>();

    //添加评论
    @Override
    public int saveComment(Comment comment) {
        comment.setCreateTime(new Date());
        return commentMapper.saveComment(comment);
    }

    //根据信息id 查询评论
    @Override
    public List<Comment> listComment(Long infoId) {
        //查询出信息的所有父节点,-1
        List<Comment> comments  = commentMapper.findListCommentByParent(infoId,Long.parseLong("-1"));
       //查询每一个父节点是否有子节点
        for (Comment comment: comments) {
            Long id = comment.getId();//得到父节点的id
            String parentName=comment.getNickname();//得到父节点的名字
            //得到子节点
            List<Comment> childComments  = commentMapper.findListCommentByParent(infoId, id);
            combineChildren(childComments,parentName,infoId);
            comment.setReplyComments(tempReplys);
            tempReplys=new ArrayList<>();//重新清空回复的评论
        }

        return comments;
    }

    //封装-1评论的子评论
   private void combineChildren(List<Comment> childComments,String parentName,Long infoId){
            if (childComments.size()>0){
                for (Comment oneComment:childComments) {
                    oneComment.setParentCommentName(parentName);//设置父级名字
                    //TODO：可以直接setparentComment,把父级付给子集
                    tempReplys.add(oneComment);
                    //得到自己的id和名字，方便赋给下一级
                    String nickname = oneComment.getNickname();
                    Long childId = oneComment.getId();
                    //查询二级以及所有子集回复
                    recursively(childId, nickname,infoId);
                }
            }
   }
    //递归封装二级以下的评论,注意：二级、三级、都是放在一起的，所以用tempReplys.add()；
    //以后有时间再做出一级存二级，二级存三级.....
    private  void recursively(Long id,String parentName,Long infoId){
        //根据id查询下一级的评论
        List<Comment> replayComments  = commentMapper.findListCommentByParent(infoId, id);
        if (replayComments.size()>0){
            for (Comment comment:replayComments) {
                comment.setParentCommentName(parentName);//设置父级名字
                //TODO：可以直接setparentComment,把父级付给子集
                tempReplys.add(comment);
                //得到自己的id和名字，方便赋给下一级
                String nickname = comment.getNickname();
                Long childId = comment.getId();
                //查询二级以及所有子集回复
                recursively(childId, nickname,infoId);
            }
        }

    }


}
