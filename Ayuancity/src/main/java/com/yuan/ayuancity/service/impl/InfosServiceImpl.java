package com.yuan.ayuancity.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yuan.ayuancity.entity.Infos;
import com.yuan.ayuancity.entity.dto.InfosDto;
import com.yuan.ayuancity.mapper.InfosMapper;
import com.yuan.ayuancity.service.InfosService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author asuyuan
 * @since 2023-02-07
 */
@Service
public class InfosServiceImpl extends ServiceImpl<InfosMapper, Infos> implements InfosService {
    @Resource
    private InfosMapper infosMapper;

    /**
     * 查询供求信息
     *
     * @param infoId 供求信息主键
     * @return 供求信息
     */
    @Override
    public Infos selectInfosByInfoId(Long infoId)
    {
        return infosMapper.selectInfosByInfoId(infoId);
    }

    /**
     * 查询供求信息列表
     *
     * @param infos 供求信息
     * @return 供求信息
     */
    @Override
    public PageInfo<Infos> selectInfosList(Infos infos, int page , int size)
    {
        PageHelper.startPage(page,size);
        List<Infos> list = infosMapper.selectInfosList(infos);
        PageInfo<Infos> PageInfo=new PageInfo<>(list);
        return PageInfo;
    }

    /**
     * 新增供求信息
     *
     * @param infos 供求信息
     * @return 结果
     */
    @Override
    public int insertInfos(Infos infos)
    {
        return infosMapper.insertInfos(infos);
    }

    /**
     * 修改供求信息
     *
     * @param infos 供求信息
     * @return 结果
     */
    @Override
    public int updateInfos(Infos infos)
    {
        return infosMapper.updateInfos(infos);
    }

    /**
     * 批量删除供求信息
     *
     * @param infoIds 需要删除的供求信息主键
     * @return 结果
     */
    @Override
    public int deleteInfosByInfoIds(Long[] infoIds)
    {
        return infosMapper.deleteInfosByInfoIds(infoIds);
    }

    /**
     * 删除供求信息信息
     *
     * @param infoId 供求信息主键
     * @return 结果
     */
    @Override
    public int deleteInfosByInfoId(Long infoId)
    {
        return infosMapper.deleteInfosByInfoId(infoId);
    }

    /*
     *审核信息
     */
    @Override
    public PageInfo<InfosDto> verify(Infos infos, int page, int size) {
        PageHelper.startPage(page,size);
       List<InfosDto> list= infosMapper.verifyInfos(infos);
       PageInfo<InfosDto> pageInfo=new PageInfo<>(list);
        return pageInfo;
    }

    /*
     前端获取热门推荐
   */
    @Override
    public  List<Infos> getHotInfos(Infos infos) {
        return infosMapper.getHotInfos(infos);
    }


}
