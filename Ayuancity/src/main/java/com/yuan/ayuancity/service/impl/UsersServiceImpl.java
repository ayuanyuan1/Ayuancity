package com.yuan.ayuancity.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yuan.ayuancity.entity.Admins;
import com.yuan.ayuancity.entity.Users;
import com.yuan.ayuancity.mapper.UsersMapper;
import com.yuan.ayuancity.service.UsersService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author asuyuan
 * @since 2023-02-07
 */
@Service
public class UsersServiceImpl extends ServiceImpl<UsersMapper, Users> implements UsersService {
    @Autowired
    private UsersMapper UsersMapper;

    /**
     * 查询用户
     *
     * @param userId 用户主键
     * @return 用户
     */
    @Override
    public Users selectUsersByUserId(Long userId)
    {
        return UsersMapper.selectUsersByUserId(userId);
    }

    /**
     * 查询用户列表
     *
     * @param Users 用户
     * @return 用户
     */
    @Override
    public PageInfo<Users> selectUsersList(int page, int size, Users Users)
    {
         PageHelper.startPage(page,size);
        List<Users> users = UsersMapper.selectUsersList(Users);
        PageInfo<Users> pageInfo=new PageInfo<>(users);
        return pageInfo;
    }

    /**
     * 新增用户
     *
     * @param Users 用户
     * @return 结果
     */
    @Override
    public int insertUsers(Users Users)
    {
        Users.setCreateTime(new Date());
        return UsersMapper.insertUsers(Users);
    }

    /**
     * 修改用户
     *
     * @param Users 用户
     * @return 结果
     */
    @Override
    public int updateUsers(Users Users)
    {
        Users.setUpdateTime(new Date());
        return UsersMapper.updateUsers(Users);
    }

    /**
     * 批量删除用户
     *
     * @param userIds 需要删除的用户主键
     * @return 结果
     */
    @Override
    public int deleteUsersByUserIds(Long[] userIds)
    {
        return UsersMapper.deleteUsersByUserIds(userIds);
    }

    /**
     * 删除用户信息
     *
     * @param userId 用户主键
     * @return 结果
     */
    @Override
    public int deleteUsersByUserId(Long userId)
    {
        return UsersMapper.deleteUsersByUserId(userId);
    }
}
