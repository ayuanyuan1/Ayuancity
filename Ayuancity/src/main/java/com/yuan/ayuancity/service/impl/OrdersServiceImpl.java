package com.yuan.ayuancity.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yuan.ayuancity.entity.Carts;
import com.yuan.ayuancity.entity.Orders;
import com.yuan.ayuancity.mapper.OrdersMapper;
import com.yuan.ayuancity.service.OrdersService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author asuyuan
 * @since 2023-04-25
 */
@Service
public class OrdersServiceImpl extends ServiceImpl<OrdersMapper, Orders> implements OrdersService {

    @Resource
    private OrdersMapper ordersMapper;

    //根据用户id查询订单表
    @Override
    public PageInfo<Orders> selectOrdersListbyUserId(int page, int size, Integer userId) {
        PageHelper.startPage(page,size);
        List<Orders> orders = ordersMapper.selectOrdersListbyUserId(userId);
        PageInfo<Orders> pageInfo=new PageInfo<>(orders);
        return pageInfo;
    }
}
