package com.yuan.ayuancity.service;

import com.github.pagehelper.PageInfo;
import com.yuan.ayuancity.entity.Carts;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yuan.ayuancity.entity.Users;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author asuyuan
 * @since 2023-04-25
 */
public interface CartsService extends IService<Carts> {

    //根据用户id查询购物列表
    PageInfo<Carts> selectCartsListbyUserId(int page, int size, Integer userId);
    //添加购物车
    int addCart(Integer userId, Integer infosId);
    //删除购物车
    int deleteCart(Integer cartId);
}
