package com.yuan.ayuancity.service;

import com.yuan.ayuancity.entity.Comment;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author asuyuan
 * @since 2023-04-27
 */
public interface CommentService extends IService<Comment> {

    //添加评论
    int saveComment(Comment comment);

    //查询评论列表=》根据信息id
    List<Comment> listComment(Long infoId);
}
