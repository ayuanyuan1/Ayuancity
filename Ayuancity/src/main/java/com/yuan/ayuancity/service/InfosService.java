package com.yuan.ayuancity.service;

import com.github.pagehelper.PageInfo;
import com.yuan.ayuancity.entity.Infos;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yuan.ayuancity.entity.dto.InfosDto;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author asuyuan
 * @since 2023-02-07
 */
public interface InfosService extends IService<Infos> {
    /**
     * 查询供求信息
     *
     * @param infoId 供求信息主键
     * @return 供求信息
     */
    public Infos selectInfosByInfoId(Long infoId);

    /**
     * 查询供求信息列表
     *
     * @param infos 供求信息
     * @return 供求信息集合
     */
    public PageInfo<Infos> selectInfosList(Infos infos, int page, int size);

    /**
     * 新增供求信息
     *
     * @param infos 供求信息
     * @return 结果
     */
    public int insertInfos(Infos infos);

    /**
     * 修改供求信息
     *
     * @param infos 供求信息
     * @return 结果
     */
    public int updateInfos(Infos infos);

    /**
     * 批量删除供求信息
     *
     * @param infoIds 需要删除的供求信息主键集合
     * @return 结果
     */
    public int deleteInfosByInfoIds(Long[] infoIds);

    /**
     * 删除供求信息信息
     *
     * @param infoId 供求信息主键
     * @return 结果
     */
    public int deleteInfosByInfoId(Long infoId);
    /*
     *审核信息
     */
    PageInfo<InfosDto> verify(Infos infos, int page, int size);
    /*
     前端获取热门推荐
    */
    List<Infos> getHotInfos(Infos infos);
}
