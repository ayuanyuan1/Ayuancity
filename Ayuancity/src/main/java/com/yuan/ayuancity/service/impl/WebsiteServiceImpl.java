package com.yuan.ayuancity.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yuan.ayuancity.entity.Website;
import com.yuan.ayuancity.mapper.WebsiteMapper;
import com.yuan.ayuancity.service.WebsiteService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author asuyuan
 * @since 2023-02-07
 */
@Service
public class WebsiteServiceImpl extends ServiceImpl<WebsiteMapper, Website> implements WebsiteService {

    @Autowired
    private WebsiteMapper websiteMapper;

    /**
     * 查询网站简介
     *
     * @param introduceId 网站简介主键
     * @return 网站简介
     */
    @Override
    public Website selectWebsiteByIntroduceId(Long introduceId)
    {
        return websiteMapper.selectWebsiteByIntroduceId(introduceId);
    }

    /**
     * 查询网站简介列表
     *
     * @param website 网站简介
     * @return 网站简介
     */
    @Override
    public PageInfo<Website> selectWebsiteList(Website website,int page, int size)
    {
        PageHelper.startPage(page,size);
        List<Website> websites = websiteMapper.selectWebsiteList(website);
        PageInfo<Website> pageInfo=new PageInfo<>(websites);
        return pageInfo;
    }

    /**
     * 新增网站简介
     *
     * @param website 网站简介
     * @return 结果
     */
    @Override
    public int insertWebsite(Website website)
    {
        website.setCreateTime(new Date());
        return websiteMapper.insertWebsite(website);
    }

    /**
     * 修改网站简介
     *
     * @param website 网站简介
     * @return 结果
     */
    @Override
    public int updateWebsite(Website website)
    {
        website.setUpdateTime(new Date());
        return websiteMapper.updateWebsite(website);
    }

    /**
     * 批量删除网站简介
     *
     * @param introduceIds 需要删除的网站简介主键
     * @return 结果
     */
    @Override
    public int deleteWebsiteByIntroduceIds(Long[] introduceIds)
    {
        return websiteMapper.deleteWebsiteByIntroduceIds(introduceIds);
    }

    /**
     * 删除网站简介信息
     *
     * @param introduceId 网站简介主键
     * @return 结果
     */
    @Override
    public int deleteWebsiteByIntroduceId(Long introduceId)
    {
        return websiteMapper.deleteWebsiteByIntroduceId(introduceId);
    }
}

