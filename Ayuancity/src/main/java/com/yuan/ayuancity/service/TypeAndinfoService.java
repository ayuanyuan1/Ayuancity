package com.yuan.ayuancity.service;

import java.util.List;
import com.yuan.ayuancity.entity.TypeAndinfo;

/**
 * 信息连接Service接口
 * 
 * @author suyuan
 * @date 2023-02-27
 */
public interface TypeAndinfoService 
{
    /**
     * 查询信息连接
     * 
     * @param typeInfoId 信息连接主键
     * @return 信息连接
     */
    public TypeAndinfo selectTypeAndinfoByTypeInfoId(Long typeInfoId);

    /**
     * 查询信息连接列表
     * 
     * @param typeAndinfo 信息连接
     * @return 信息连接集合
     */
    public List<TypeAndinfo> selectTypeAndinfoList(TypeAndinfo typeAndinfo);

    /**
     * 新增信息连接
     * 
     * @param typeAndinfo 信息连接
     * @return 结果
     */
    public int insertTypeAndinfo(TypeAndinfo typeAndinfo);

    /**
     * 修改信息连接
     * 
     * @param typeAndinfo 信息连接
     * @return 结果
     */
    public int updateTypeAndinfo(TypeAndinfo typeAndinfo);

    /**
     * 批量删除信息连接
     * 
     * @param typeInfoIds 需要删除的信息连接主键集合
     * @return 结果
     */
    public int deleteTypeAndinfoByTypeInfoIds(Long[] typeInfoIds);

    /**
     * 删除信息连接信息
     * 
     * @param typeInfoId 信息连接主键
     * @return 结果
     */
    public int deleteTypeAndinfoByTypeInfoId(Long typeInfoId);
}
