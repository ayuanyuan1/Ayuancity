package com.yuan.ayuancity.service.impl;

import java.util.List;

import com.yuan.ayuancity.entity.TypeAndinfo;
import com.yuan.ayuancity.mapper.TypeAndinfoMapper;
import com.yuan.ayuancity.service.TypeAndinfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
/**
 * 信息连接Service业务层处理
 * 
 * @author suyuan
 * @date 2023-02-27
 */
@Service
public class TypeAndinfoServiceImpl implements TypeAndinfoService
{
    @Autowired
    private TypeAndinfoMapper typeAndinfoMapper;

    /**
     * 查询信息连接
     * 
     * @param typeInfoId 信息连接主键
     * @return 信息连接
     */
    @Override
    public TypeAndinfo selectTypeAndinfoByTypeInfoId(Long typeInfoId)
    {
        return typeAndinfoMapper.selectTypeAndinfoByTypeInfoId(typeInfoId);
    }

    /**
     * 查询信息连接列表
     * 
     * @param typeAndinfo 信息连接
     * @return 信息连接
     */
    @Override
    public List<TypeAndinfo> selectTypeAndinfoList(TypeAndinfo typeAndinfo)
    {
        return typeAndinfoMapper.selectTypeAndinfoList(typeAndinfo);
    }

    /**
     * 新增信息连接
     * 
     * @param typeAndinfo 信息连接
     * @return 结果
     */
    @Override
    public int insertTypeAndinfo(TypeAndinfo typeAndinfo)
    {
        return typeAndinfoMapper.insertTypeAndinfo(typeAndinfo);
    }

    /**
     * 修改信息连接
     * 
     * @param typeAndinfo 信息连接
     * @return 结果
     */
    @Override
    public int updateTypeAndinfo(TypeAndinfo typeAndinfo)
    {
        return typeAndinfoMapper.updateTypeAndinfo(typeAndinfo);
    }

    /**
     * 批量删除信息连接
     * 
     * @param typeInfoIds 需要删除的信息连接主键
     * @return 结果
     */
    @Override
    public int deleteTypeAndinfoByTypeInfoIds(Long[] typeInfoIds)
    {
        return typeAndinfoMapper.deleteTypeAndinfoByTypeInfoIds(typeInfoIds);
    }

    /**
     * 删除信息连接信息
     * 
     * @param typeInfoId 信息连接主键
     * @return 结果
     */
    @Override
    public int deleteTypeAndinfoByTypeInfoId(Long typeInfoId)
    {
        return typeAndinfoMapper.deleteTypeAndinfoByTypeInfoId(typeInfoId);
    }
}
