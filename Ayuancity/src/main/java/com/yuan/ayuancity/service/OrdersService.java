package com.yuan.ayuancity.service;

import com.github.pagehelper.PageInfo;
import com.yuan.ayuancity.entity.Orders;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author asuyuan
 * @since 2023-04-25
 */
public interface OrdersService extends IService<Orders> {

    //根据用户id查询订单表
    PageInfo<Orders> selectOrdersListbyUserId(int page, int size, Integer userId);
}
