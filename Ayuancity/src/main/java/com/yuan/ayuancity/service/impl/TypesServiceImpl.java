package com.yuan.ayuancity.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yuan.ayuancity.entity.Infos;
import com.yuan.ayuancity.entity.Types;
import com.yuan.ayuancity.mapper.TypesMapper;
import com.yuan.ayuancity.service.TypesService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author asuyuan
 * @since 2023-02-07
 */
@Service
public class TypesServiceImpl extends ServiceImpl<TypesMapper, Types> implements TypesService {
    @Autowired
    private TypesMapper typesMapper;

    /**
     * 查询供求信息二级种类
     *
     * @param typeId 供求信息二级种类主键
     * @return 供求信息二级种类
     */
    @Override
    public Types selectTypesByTypeId(Long typeId)
    {
        return typesMapper.selectTypesByTypeId(typeId);
    }

    /**
     * 查询供求信息二级种类列表
     *
     * @param types 供求信息二级种类
     * @return 供求信息二级种类
     */
    @Override
    public PageInfo<Types> selectTypesList(int page , int size, Types types)
    {
        PageHelper.startPage(page,size);
        List<Types> typesList = typesMapper.selectTypesList2(types);
        PageInfo<Types> typesPageInfo=new PageInfo<>(typesList);
        return typesPageInfo;
    }

    /**
     * 新增供求信息二级种类
     *
     * @param types 供求信息二级种类
     * @return 结果
     */
    @Override
    public int insertTypes(Types types)
    {
        return typesMapper.insertTypes(types);
    }

    /**
     * 修改供求信息二级种类
     *
     * @param types 供求信息二级种类
     * @return 结果
     */
    @Override
    public int updateTypes(Types types)
    {
        return typesMapper.updateTypes(types);
    }

    /**
     * 批量删除供求信息二级种类
     *
     * @param typeIds 需要删除的供求信息二级种类主键
     * @return 结果
     */
    @Override
    public int deleteTypesByTypeIds(Long[] typeIds)
    {
        return typesMapper.deleteTypesByTypeIds(typeIds);
    }

    /**
     * 删除供求信息二级种类信息
     *
     * @param typeId 供求信息二级种类主键
     * @return 结果
     */
    @Override
    public int deleteTypesByTypeId(Long typeId)
    {
        return typesMapper.deleteTypesByTypeId(typeId);
    }

    @Override
    public List<Types> selectTypesAllList(Types types) {
        return typesMapper.selectTypesList2(types);
    }
}
