package com.yuan.ayuancity.service;

import com.github.pagehelper.PageInfo;
import com.yuan.ayuancity.entity.Infos;
import com.yuan.ayuancity.entity.Types;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author asuyuan
 * @since 2023-02-07
 */
public interface TypesService extends IService<Types> {

    /**
     * 查询供求信息二级种类
     *
     * @param typeId 供求信息二级种类主键
     * @return 供求信息二级种类
     */
    public Types selectTypesByTypeId(Long typeId);

    /**
     * 查询供求信息二级种类列表
     *
     * @param types 供求信息二级种类
     * @return 供求信息二级种类集合
     */
    public PageInfo<Types> selectTypesList(int page, int size, Types types);

    /**
     * 新增供求信息二级种类
     *
     * @param types 供求信息二级种类
     * @return 结果
     */
    public int insertTypes(Types types);

    /**
     * 修改供求信息二级种类
     *
     * @param types 供求信息二级种类
     * @return 结果
     */
    public int updateTypes(Types types);

    /**
     * 批量删除供求信息二级种类
     *
     * @param typeIds 需要删除的供求信息二级种类主键集合
     * @return 结果
     */
    public int deleteTypesByTypeIds(Long[] typeIds);

    /**
     * 删除供求信息二级种类信息
     *
     * @param typeId 供求信息二级种类主键
     * @return 结果
     */
    public int deleteTypesByTypeId(Long typeId);

    List<Types> selectTypesAllList(Types types);
}

