package com.yuan.ayuancity.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yuan.ayuancity.entity.Admins;
import com.yuan.ayuancity.mapper.AdminsMapper;
import com.yuan.ayuancity.service.AdminsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resources;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author asuyuan
 * @since 2023-02-07
 */
@Service
public class AdminsServiceImpl extends ServiceImpl<AdminsMapper, Admins> implements AdminsService {

    @Autowired
    private AdminsMapper AdminsMapper;

    /**
     * 查询管理员
     *
     * @param adminId 管理员主键
     * @return 管理员
     */
    @Override
    public Admins selectAdminsByAdminId(Long adminId)
    {
        return AdminsMapper.selectAdminsByAdminId(adminId);
    }

    /**
     * 查询管理员列表
     *
     * @param Admins 管理员
     * @return 管理员
     */
    @Override
    public PageInfo<Admins> selectAdminsList(int page, int size, Admins Admins)
    {
        PageHelper.startPage(page,size);
        List<Admins> admins = AdminsMapper.selectAdminsList(Admins);
        PageInfo<Admins> adminsPageInfo = new PageInfo<>(admins);
        return adminsPageInfo;
    }

    /**
     * 新增管理员
     *
     * @param Admin 管理员
     * @return 结果
     */
    @Override
    public int insertAdmins(Admins Admin)
    {
        Admin.setCreateTime(new Date());
        return AdminsMapper.insertAdmins(Admin);
    }

    /**
     * 修改管理员
     *
     * @param Admin 管理员
     * @return 结果
     */
    @Override
    public int updateAdmins(Admins Admin)
    {
        Admin.setUpdateTime(new Date());
        return AdminsMapper.updateAdmins(Admin);
    }

    /**
     * 批量删除管理员
     *
     * @param adminIds 需要删除的管理员主键
     * @return 结果
     */
    @Override
    public int deleteAdminsByAdminIds(Long[] adminIds)
    {
        return AdminsMapper.deleteAdminsByAdminIds(adminIds);
    }

    /**
     * 删除管理员信息
     *
     * @param adminId 管理员主键
     * @return 结果
     */
    @Override
    public int deleteAdminsByAdminId(Long adminId)
    {
        return AdminsMapper.deleteAdminsByAdminId(adminId);
    }
}
