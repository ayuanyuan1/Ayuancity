package com.yuan.ayuancity.service;

import com.github.pagehelper.PageInfo;
import com.yuan.ayuancity.entity.Admins;
import com.baomidou.mybatisplus.extension.service.IService;
import javafx.scene.control.Pagination;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author asuyuan
 * @since 2023-02-07
 */
public interface AdminsService extends IService<Admins> {
    /**
     * 查询管理员
     *
     * @param adminId 管理员主键
     * @return 管理员
     */
    public Admins selectAdminsByAdminId(Long adminId);

    /**
     * 查询管理员列表
     *
     * @param Admins 管理员
     * @return 管理员集合
     */
    public PageInfo<Admins> selectAdminsList(int page, int size, Admins Admins);

    /**
     * 新增管理员
     *
     * @param Admins 管理员
     * @return 结果
     */
    public int insertAdmins(Admins Admins);

    /**
     * 修改管理员
     *
     * @param Admins 管理员
     * @return 结果
     */
    public int updateAdmins(Admins Admins);

    /**
     * 批量删除管理员
     *
     * @param adminIds 需要删除的管理员主键集合
     * @return 结果
     */
    public int deleteAdminsByAdminIds(Long[] adminIds);

    /**
     * 删除管理员信息
     *
     * @param adminId 管理员主键
     * @return 结果
     */
    public int deleteAdminsByAdminId(Long adminId);
}

