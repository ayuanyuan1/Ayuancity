package com.yuan.ayuancity.service;

import com.github.pagehelper.PageInfo;
import com.yuan.ayuancity.entity.Users;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author asuyuan
 * @since 2023-02-07
 */
public interface UsersService extends IService<Users> {
    /**
     * 查询用户
     *
     * @param userId 用户主键
     * @return 用户
     */
    public Users selectUsersByUserId(Long userId);

    /**
     * 查询用户列表
     *
     * @param Users 用户
     * @return 用户集合
     */
    public PageInfo<Users> selectUsersList(int page, int size, Users Users);

    /**
     * 新增用户
     *
     * @param Users 用户
     * @return 结果
     */
    public int insertUsers(Users Users);

    /**
     * 修改用户
     *
     * @param Users 用户
     * @return 结果
     */
    public int updateUsers(Users Users);

    /**
     * 批量删除用户
     *
     * @param userIds 需要删除的用户主键集合
     * @return 结果
     */
    public int deleteUsersByUserIds(Long[] userIds);

    /**
     * 删除用户信息
     *
     * @param userId 用户主键
     * @return 结果
     */
    public int deleteUsersByUserId(Long userId);
}
