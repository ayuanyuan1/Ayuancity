package com.yuan.ayuancity.service;

import com.github.pagehelper.PageInfo;
import com.yuan.ayuancity.entity.Website;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author asuyuan
 * @since 2023-02-07
 */
public interface WebsiteService extends IService<Website> {

    /**
     * 查询网站简介
     *
     * @param introduceId 网站简介主键
     * @return 网站简介
     */
    public Website selectWebsiteByIntroduceId(Long introduceId);

    /**
     * 查询网站简介列表
     *
     * @param website 网站简介
     * @return 网站简介集合
     */
    public PageInfo<Website> selectWebsiteList(Website website,int page ,int size);

    /**
     * 新增网站简介
     *
     * @param website 网站简介
     * @return 结果
     */
    public int insertWebsite(Website website);

    /**
     * 修改网站简介
     *
     * @param website 网站简介
     * @return 结果
     */
    public int updateWebsite(Website website);

    /**
     * 批量删除网站简介
     *
     * @param introduceIds 需要删除的网站简介主键集合
     * @return 结果
     */
    public int deleteWebsiteByIntroduceIds(Long[] introduceIds);

    /**
     * 删除网站简介信息
     *
     * @param introduceId 网站简介主键
     * @return 结果
     */
    public int deleteWebsiteByIntroduceId(Long introduceId);
}
