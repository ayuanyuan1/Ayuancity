package com.yuan.ayuancity.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yuan.ayuancity.entity.*;
import com.yuan.ayuancity.mapper.CartsMapper;
import com.yuan.ayuancity.mapper.InfosMapper;
import com.yuan.ayuancity.mapper.TypeAndinfoMapper;
import com.yuan.ayuancity.service.CartsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yuan.ayuancity.service.TypeAndinfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author asuyuan
 * @since 2023-04-25
 */
@Service
public class CartsServiceImpl extends ServiceImpl<CartsMapper, Carts> implements CartsService {
  @Resource
  private CartsMapper cartsMapper;
  @Resource
  private TypeAndinfoMapper typeAndinfoMapper;
    //根据用户id查询购物列表
    @Override
    public PageInfo<Carts> selectCartsListbyUserId(int page, int size, Integer userId) {
        PageHelper.startPage(page,size);
        List<Carts> carts = cartsMapper.selectCartsListbyUserId(userId);
        PageInfo<Carts> pageInfo=new PageInfo<>(carts);
        return pageInfo;
    }
    //添加购物车
    @Override
    public int addCart(Integer userId, Integer infosId) {
        //查找信息分类
        QueryWrapper<TypeAndinfo> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("info_id", infosId);
        TypeAndinfo typeAndinfo = typeAndinfoMapper.selectOne(queryWrapper);
        return cartsMapper.addCart(userId,infosId,typeAndinfo.getTypeId());
    }

    @Override
    public int deleteCart(Integer cartId) {
        return cartsMapper.deleteById(cartId);
    }
}
