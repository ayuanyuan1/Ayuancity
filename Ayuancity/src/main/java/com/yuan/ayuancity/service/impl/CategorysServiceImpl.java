package com.yuan.ayuancity.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yuan.ayuancity.entity.Categorys;
import com.yuan.ayuancity.entity.dto.CategorysDto;
import com.yuan.ayuancity.mapper.CategorysMapper;
import com.yuan.ayuancity.service.CategorysService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author asuyuan
 * @since 2023-02-07
 */
@Service
public class CategorysServiceImpl extends ServiceImpl<CategorysMapper, Categorys> implements CategorysService {
    @Autowired
    private CategorysMapper categorysMapper;

    /**
     * 查询供求信息类型
     *
     * @param categoryId 供求信息类型主键
     * @return 供求信息类型
     */
    @Override
    public Categorys selectCategorysByCategoryId(Long categoryId)
    {
        return categorysMapper.selectCategorysByCategoryId(categoryId);
    }

    /**
     * 查询供求信息类型列表
     *
     * @param categorys 供求信息类型
     * @return 供求信息类型
     */
    @Override
    public PageInfo<Categorys> selectCategorysList(int page , int size, Categorys categorys)
    {
        PageHelper.startPage(page,size);
        List<Categorys> categorysList = categorysMapper.selectCategorysList(categorys);
        PageInfo<Categorys> pageInfo=new PageInfo<>(categorysList);
        return pageInfo;
    }

    /**
     * 新增供求信息类型
     *
     * @param categorys 供求信息类型
     * @return 结果
     */
    @Override
    public int insertCategorys(Categorys categorys)
    {
        return categorysMapper.insertCategorys(categorys);
    }

    /**
     * 修改供求信息类型
     *
     * @param categorys 供求信息类型
     * @return 结果
     */
    @Override
    public int updateCategorys(Categorys categorys)
    {
        return categorysMapper.updateCategorys(categorys);
    }

    /**
     * 批量删除供求信息类型
     *
     * @param categoryIds 需要删除的供求信息类型主键
     * @return 结果
     */
    @Override
    public int deleteCategorysByCategoryIds(Long[] categoryIds)
    {
        return categorysMapper.deleteCategorysByCategoryIds(categoryIds);
    }

    /**
     * 删除供求信息类型信息
     *
     * @param categoryId 供求信息类型主键
     * @return 结果
     */
    @Override
    public int deleteCategorysByCategoryId(Long categoryId)
    {
        return categorysMapper.deleteCategorysByCategoryId(categoryId);
    }
    /*
      页面导航栏
     */
    @Override
    public List<CategorysDto> getcategoryAndtype() {
        return categorysMapper.getcategoryAndtype();
    }
}

