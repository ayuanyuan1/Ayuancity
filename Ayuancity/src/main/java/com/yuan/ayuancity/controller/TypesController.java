package com.yuan.ayuancity.controller;


import com.github.pagehelper.PageInfo;
import com.yuan.ayuancity.entity.Infos;
import com.yuan.ayuancity.entity.Types;
import com.yuan.ayuancity.service.TypesService;
import com.yuan.view.YuancityView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author asuyuan
 * @since 2023-02-07
 */
@RestController
@RequestMapping("/ayuancity/types")
@CrossOrigin
public class TypesController {

    @Autowired
    private TypesService typesService;

    /**
     * 查询供求信息二级种类列表
     */
    @GetMapping("/list")
    public YuancityView list(Types types,
                             @RequestParam(value ="pageNum",required = false,defaultValue = "1")int page,
                             @RequestParam(value="pageSize",required = false,defaultValue = "5")int size)
    {
       PageInfo<Types> list = typesService.selectTypesList(page,size,types);
        return YuancityView.success("查询成功").put("data",list);
    }
    @GetMapping("/listAll")
    public YuancityView listAll(Types types)
    {
        List<Types> list = typesService.selectTypesAllList(types);
        return YuancityView.success("查询成功").put("data", list);
    }


    /**
     * 获取供求信息二级种类详细信息
     */
    @GetMapping(value = "/{typeId}")
    public YuancityView getInfo(@PathVariable("typeId") Long typeId)
    {
        Types types = typesService.selectTypesByTypeId(typeId);
        return YuancityView.success("查询成功").put("data", types);
    }

    /**
     * 新增供求信息二级种类
     */
    @PostMapping
    public YuancityView add(@RequestBody Types types)
    {typesService.insertTypes(types);
        return YuancityView.success("新增成功");
    }

    /**
     * 修改供求信息二级种类
     */
    @PutMapping
    public YuancityView edit(@RequestBody Types types)
    {
        typesService.updateTypes(types);
        return YuancityView.success("修改成功");
    }

    /**
     * 删除供求信息二级种类
     */
    @DeleteMapping("/{typeIds}")
    public YuancityView remove(@PathVariable Long[] typeIds)
    {typesService.deleteTypesByTypeIds(typeIds);
        return YuancityView.success("删除成功");
    }
}

