package com.yuan.ayuancity.controller.common;

import com.wf.captcha.SpecCaptcha;
import com.yuan.view.YuancityView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 *    用户注册邮箱验证，以后改成短信验证
 * </p>
 *
 * @author asuyuan
 * @since 2023/5/2
 */
@RestController
@RequestMapping("/city/v1")
public class EmailController {


    @Resource
    JavaMailSender javaMailSender;

  @GetMapping("/SendEmail")
   public YuancityView SendEmail(HttpServletRequest request, @RequestParam String userEmail){
      //获取6位数验证码
      SpecCaptcha code = new SpecCaptcha(130, 48, 6);//字母数字
      String Emailcode = code.text();
      System.out.println("邮箱验证码 :" + Emailcode);

      SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
      simpleMailMessage.setFrom("2686030981@qq.com");//"发送者邮件账号"
      simpleMailMessage.setSubject("YuanCity注册");//"邮件标题"
      simpleMailMessage.setText("亲爱的会员： 您好！\n" +
              "您正在注册验证，请在验证码输入框中输入： "+Emailcode+"，以完成操作。\n" +
              "注意：此操作可能会修改您的密码、登录邮箱或绑定手机。如非本人操作，请及时登录并修改密码以保证帐户安全\n" +
              "（工作人员不会向你索取此验证码，请勿泄漏！)");//"邮件内容"
      simpleMailMessage.setTo(userEmail);//"收件人邮件账号"
      javaMailSender.send(simpleMailMessage);

      //存session或者redis
      request.getSession().setAttribute("eamilcode", Emailcode);
      request.getSession().setMaxInactiveInterval(60 * 3);//设置session过期时间，以秒为单位
      return YuancityView.success("发送成功");
   }


}
