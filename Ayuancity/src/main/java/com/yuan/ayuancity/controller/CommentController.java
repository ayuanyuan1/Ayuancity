package com.yuan.ayuancity.controller;


import com.yuan.ayuancity.entity.Comment;
import com.yuan.ayuancity.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author asuyuan
 * @since 2023-04-27
 */
@Controller
@RequestMapping("/ayuancity/comment")
public class CommentController {

    @Autowired
    private CommentService commentService;

    //根据id查询评论
    @GetMapping("/infoComment/{infoId}")
     public String infoComment(Model model,@PathVariable String infoId){
        List<Comment> comments = commentService.listComment(Long.parseLong(infoId));
        model.addAttribute("comments",comments);
        return "itemDetail :: commentList";
    }

    //添加评论
    @PostMapping(value = "/AddinfoComment")
    public String AddinfoComment(Comment comment) {
        //设置头像
        comment.setAvatar(comment.getAvatar());
        if (comment.getParentComment().getId() != null) {
            comment.setParentCommentId(comment.getParentComment().getId());
        }
        commentService.saveComment(comment);
        return "redirect:/ayuancity/comment/infoComment/"+comment.getInfoId();
    }

}

