package com.yuan.ayuancity.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sun.javafx.collections.MappingChange;
import com.yuan.ayuancity.entity.Admins;
import com.yuan.ayuancity.entity.Users;
import com.yuan.ayuancity.mapper.UsersMapper;
import com.yuan.ayuancity.service.UsersService;
import com.yuan.security.SecurityUser;
import com.yuan.view.JsonHelper;
import com.yuan.view.JwtHelper;
import com.yuan.view.YuancityView;
import io.jsonwebtoken.Claims;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

/**
 * <p>
 * 登录与退出
 * </p>
 *
 * @author asuyuan
 * @since 2023/2/8
 */
@Api(tags = "登录与退出控制器")
@RestController
@RequestMapping("/city")
@CrossOrigin
public class LoginController {

    @Autowired
    UsersService usersService;
    @Resource
    UsersMapper usersMapper;

    @ApiOperation(value = "用户登录")
    @PostMapping("/user/login")
    public YuancityView UserLogin(String userAccount,String userPassword){
        if (userAccount==null||userAccount.equals("")){
            return YuancityView.failure("用户名为空");
        }
        QueryWrapper<Users>queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("user_account",userAccount);
         Users datauser = usersMapper.selectOne(queryWrapper);
        if (datauser==null){
            return YuancityView.failure("该用户不存在");
        }
        //校验密码
         boolean checkpw = BCrypt.checkpw(userPassword, datauser.getUserPassword());
        if (checkpw==false){
            return YuancityView.failure("密码错误");
        }
        //封装JWT
        JwtHelper jwtHelper=new JwtHelper();
        SecurityUser details=new SecurityUser(datauser);
        String userToken = jwtHelper.createUserToken(details, new Users());
        Claims claimsByToken = jwtHelper.getClaimsByToken(userToken);
        System.out.println("用户token==="+claimsByToken);

       return YuancityView.success("登录成功").put("token", userToken);
    }

    @ApiOperation(value = "获取用户token")
    @GetMapping("/user/info")
    public YuancityView UserToken(String token){
        //JWT获取token信息
        JwtHelper jwtHelper=new JwtHelper();
        Claims claimsByToken = jwtHelper.getClaimsByToken(token);
        HashMap user = claimsByToken.get("user", HashMap.class);
        //转成bean
        String userJson = JsonHelper.toJSON(user);
        Users users = JsonHelper.toBean(userJson, Users.class);
        return YuancityView.success("token信息~")
                .put("name",users.getUserName())
                .put("roles", "user")
                .put("avatar",users.getUserImg())
                .put("user",users);
    }
    @ApiOperation(value = "用户退出登录")
    @PostMapping("/user/OutLogin")
    public YuancityView UserLoginout(){

        return YuancityView.success("退出成功,删除token");

    }

    @ApiOperation(value = "管理员登录")
    // @PostMapping("/admin/login")
    public YuancityView AdminLogin(String username,String password){
        return YuancityView.success("登录成功,存放token~").put("token", "admin");

    }
    @ApiOperation(value = "获取管理员token")
    @GetMapping("/admin/info")
    public YuancityView AdminToken(String token){
        //JWT获取token信息
        JwtHelper jwtHelper=new JwtHelper();
        Claims claimsByToken = jwtHelper.getClaimsByToken(token);
        HashMap admin = claimsByToken.get("admin", HashMap.class);
        //转成bean
        String adminJson = JsonHelper.toJSON(admin);
        Admins admins = JsonHelper.toBean(adminJson, Admins.class);
        return YuancityView.success("token信息~")
                .put("name",admins.getAdminAccount())
                .put("roles", "admin")
                .put("avatar",admins.getAdminImg())
                .put("admin",admins);
    }

    @ApiOperation(value = "管理员退出")
    // @PostMapping("/admin/logout")
    public YuancityView AdminLogOut(){
        return YuancityView.success("退出成功~").put("token","");
    }


}
