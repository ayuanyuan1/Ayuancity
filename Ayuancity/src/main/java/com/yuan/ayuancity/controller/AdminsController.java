package com.yuan.ayuancity.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.mysql.cj.protocol.ExportControlled;
import com.yuan.ayuancity.entity.Admins;
import com.yuan.ayuancity.mapper.AdminsMapper;
import com.yuan.ayuancity.service.AdminsService;
import com.yuan.view.BcryptHelper;
import com.yuan.view.YuancityView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author asuyuan
 * @since 2023-02-07
 */
@Api(tags = "管理员控制器")
@RestController
@RequestMapping("/ayuancity/admins")
@CrossOrigin
public class AdminsController {
    @Autowired
    private AdminsService AdminsService;
    @Resource
    private AdminsMapper adminsMapper;

    /**
     * 查询管理员列表
     */
    @ApiOperation("管理员列表")
    @GetMapping("/list")
    public YuancityView list(Admins Admins,
                             @RequestParam(value ="pageNum",required = false,defaultValue = "1")int page,
                             @RequestParam(value="pageSize",required = false,defaultValue = "5")int size)
    {
        PageInfo<Admins> list = AdminsService.selectAdminsList(page,size,Admins);
        return YuancityView.success("查询成功").put("data", list);
    }


    /**
     * 获取管理员详细信息
     */
    @GetMapping(value = "/{adminId}")
    public YuancityView getInfo(@PathVariable("adminId") Long adminId)
    {
        return YuancityView.success("查询成功").put("data", AdminsService.selectAdminsByAdminId(adminId));
    }

    /**
     * 新增管理员
     */

    @PostMapping
    public YuancityView add(@RequestBody Admins Admins)
    {
         QueryWrapper<Admins> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("admin_account", Admins.getAdminAccount());
        Admins oldamin = adminsMapper.selectOne(queryWrapper);
        if (oldamin!=null){
            return YuancityView.failure("账户已存在");
        }
        //加密
        String encrypt = BcryptHelper.bcrypt(Admins.getAdminPassword());
        Admins.setAdminPassword(encrypt);
        AdminsService.insertAdmins(Admins);
        return YuancityView.success("添加成功");
    }

    /**
     * 修改管理员
     */
    @PutMapping
    public YuancityView edit(@RequestBody Admins tbAdmins)
    {
        //TODO 目前打算查询数据库密码与现在密码是否一致，不一致则修改==》加权限，非admin不给看修改密码框
        //TODO 后期把修改密码单独做个接口
        Admins oldAdmin = AdminsService.selectAdminsByAdminId(tbAdmins.getAdminId().longValue());
        //BcryptHelper.matches( tbAdmins.getAdminPassword(),oldAdmin.getAdminPassword())
        // 前端是已经加密过了
        if (!oldAdmin.getAdminPassword().equals( tbAdmins.getAdminPassword())){
            //前端传过来的密码和数据库不一致，则加密前端的密码
            String encrypt = BcryptHelper.bcrypt(tbAdmins.getAdminPassword());
            tbAdmins.setAdminPassword(encrypt);
        }
        AdminsService.updateAdmins(tbAdmins);
        return YuancityView.success("修改成功");
    }

    /**
     * 删除管理员
     */
    @DeleteMapping("/{adminIds}")
    public YuancityView remove(@PathVariable Long[] adminIds)
    {   AdminsService.deleteAdminsByAdminIds(adminIds);
        return YuancityView.success("删除成功");
    }
    /**
     * 修改密码
     */
    @PostMapping("/editAdminPwd")
    public YuancityView editAdminPwd(String adminId,String oldpwd,String newpwd)
    {
        //查询数据库
        Admins admins = AdminsService.selectAdminsByAdminId(Long.parseLong(adminId));
        //比较
        if (BcryptHelper.matches( oldpwd,admins.getAdminPassword())){
            //前端传过来的密码和数据库一直
            String encrypt = BcryptHelper.bcrypt(newpwd);
           //修改
            Admins admins1=new Admins();
            admins1.setAdminId(Integer.parseInt(adminId));
            admins1.setAdminPassword(encrypt);
            AdminsService.updateAdmins(admins1);
            return YuancityView.success("修改成功");
        }
        return YuancityView.failure("密码错误");
    }
}

