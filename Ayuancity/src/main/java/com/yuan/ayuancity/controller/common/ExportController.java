package com.yuan.ayuancity.controller.common;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.mysql.cj.protocol.ExportControlled;
import com.yuan.ayuancity.entity.Admins;
import com.yuan.ayuancity.entity.Users;
import com.yuan.ayuancity.service.AdminsService;
import com.yuan.ayuancity.service.UsersService;
import com.yuan.view.YuancityView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

/**
 * <p>
 *     导出EXCEL控制器
 * </p>
 *
 * @author asuyuan
 * @since 2023/2/17
 */
@RestController
@RequestMapping("/city/v1")
@CrossOrigin
public class ExportController {
    @Autowired
    private AdminsService AdminsService;
    @Autowired
    private UsersService usersService;

    /**
     * 管理员信息导出
     * 返回值必须为void,不然HttpServletResponse会出错
     * 前端传入null是字符串
     */
    @RequestMapping("/getAdminExcel")
    public void getAdminExcel(HttpServletResponse response,Admins admin){
        QueryWrapper queryWrapper=new QueryWrapper();
        if (!admin.getAdminName().equals("null")){
            queryWrapper.eq("admin_name", admin.getAdminName());
        }
        if (!admin.getAdminAccount().equals("null")){
            queryWrapper.eq("admin_account", admin.getAdminAccount());
        }
        if (!admin.getAdminPhone().equals("null")){
            queryWrapper.eq("admin_phone", admin.getAdminPhone());
        }
        List<Admins> admins = AdminsService.list(queryWrapper);
        try {
            export(Admins.class,admins,"管理员信息", response);
        } catch (IOException e) {
            e.printStackTrace();
        }
        // return YuancityView.success("导出成功");
    }
    @RequestMapping("/getUserExcel")
    public void getUserExcel(HttpServletResponse response, Users user){
        QueryWrapper queryWrapper=new QueryWrapper();
        if (!user.getUserName().equals("null")){
            queryWrapper.eq("user_name", user.getUserName());
        }
        if (!user.getUserAccount().equals("null")){
            queryWrapper.eq("user_account", user.getUserAccount());
        }
        if (!user.getUserPhone().equals("null")){
            queryWrapper.eq("user_phone", user.getUserPhone());
        }
        List<Users> Users = usersService.list(queryWrapper);
        try {
            export(Users.class,Users,"用户信息", response);
        } catch (IOException e) {
            e.printStackTrace();
        }
        // return YuancityView.success("导出成功");
    }

    //导出方法
    public <T> void export(Class<T> classname,List<T> listdata,String filename,HttpServletResponse response) throws IOException {
        //设置响应内容类型
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        //编码
        response.setCharacterEncoding("utf-8");
        // 设置文件名, ps:把字符串中所有的'+'替换成'%20'，在URL中%20代表空格
        String fileName = URLEncoder.encode(filename, "UTF-8").replaceAll("\\+", "%20");
        response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");//设置响应头
        //获取写出流 classname=Admins.class
        ExcelWriter writer = EasyExcel.write(response.getOutputStream(), classname).build();
        //创建表格，设置表格页名称
        WriteSheet sheet = EasyExcel.writerSheet("suyuan").build();
        writer.write(listdata, sheet);//读出
        writer.finish();//关闭流
    }

    public static <T> void  getclass(Class<T> classname){
        System.out.println(classname);
    }
    // public static void main(String[] args) {
    //
    //     Admins admins=new Admins();
    //     getclass(admins.getClass());//getClass用于实例化的变量
    //     getclass(Admins.class);//.class 用于类名
    // }

}
