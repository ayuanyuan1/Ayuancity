package com.yuan.ayuancity.controller;


import com.github.pagehelper.PageInfo;
import com.yuan.ayuancity.entity.Categorys;
import com.yuan.ayuancity.entity.dto.CategorysDto;
import com.yuan.ayuancity.service.CategorysService;
import com.yuan.view.YuancityView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author asuyuan
 * @since 2023-02-07
 */
@RestController
@RequestMapping("/ayuancity/categorys")
// @CrossOrigin
public class CategorysController {
    @Autowired
    private CategorysService categorysService;

    /**
     * 查询供求信息类型列表
     */
    @GetMapping("/list")
    public YuancityView list(Categorys categorys,
                             @RequestParam(value ="pageNum",required = false,defaultValue = "1")int page,
                             @RequestParam(value="pageSize",required = false,defaultValue = "5")int size)
    {
       PageInfo<Categorys> categorysPageInfo= categorysService.selectCategorysList(page,size,categorys);
       return YuancityView.success("查询成功").put("data", categorysPageInfo);
    }

    @GetMapping("/listAll")
    public YuancityView listAll()
    {
        List<Categorys> list = categorysService.list(null);
        return YuancityView.success("查询成功").put("data", list);
    }
   /*
     页面导航栏
    */
    @GetMapping("/getcategoryAndtype")
    public YuancityView getcategoryAndtype()
    {
        List<CategorysDto> list = categorysService.getcategoryAndtype();
        return YuancityView.success("查询成功").put("data", list);
    }


    /**
     * 获取供求信息类型详细信息
     */
    @GetMapping(value = "/{categoryId}")
    public YuancityView getInfo(@PathVariable("categoryId") Long categoryId)
    {
        Categorys categorys = categorysService.selectCategorysByCategoryId(categoryId);
        return YuancityView.success("查询成功").put("data",categorys);
    }

    /**
     * 新增供求信息类型
     */
    @PostMapping
    public YuancityView add(@RequestBody Categorys categorys)
    {categorysService.insertCategorys(categorys);
        return YuancityView.success("添加成功");
    }

    /**
     * 修改供求信息类型
     */
    @PutMapping
    public YuancityView edit(@RequestBody Categorys categorys)
    {categorysService.updateCategorys(categorys);
        return YuancityView.success("修改成功");
    }

    /**
     * 删除供求信息类型
     */
    @DeleteMapping("/{categoryIds}")
    public YuancityView remove(@PathVariable Long[] categoryIds)
    {
        categorysService.deleteCategorysByCategoryIds(categoryIds);
        return YuancityView.success("删除成功");
    }
}


