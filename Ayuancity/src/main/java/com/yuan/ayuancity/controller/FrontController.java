package com.yuan.ayuancity.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * <p>
 *   前台页面控制器
 * </p>
 *
 * @author asuyuan
 * @since 2023/4/16
 */
@Controller
@RequestMapping("/ayuancity/front")
public class FrontController {


    @RequestMapping("/search")
    public String search(){
        return "/view/html/itemsPage.html";
    }
    @RequestMapping("/index")
    public String index(){
        return "/view/html/index.html";
    }
}
