package com.yuan.ayuancity.controller;


import com.github.pagehelper.PageInfo;
import com.yuan.ayuancity.entity.Carts;
import com.yuan.ayuancity.entity.Orders;
import com.yuan.ayuancity.service.OrdersService;
import com.yuan.view.YuancityView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author asuyuan
 * @since 2023-04-25
 */
@RestController
@RequestMapping("/ayuancity/orders")
public class OrdersController {

    @Autowired
    private OrdersService ordersService;

    //根据用户id查询订单表
    @GetMapping("/list/{userId}")
   public YuancityView list(@PathVariable Integer userId,
                            @RequestParam(value ="pageNum",required = false,defaultValue = "1")int page,
                            @RequestParam(value="pageSize",required = false,defaultValue = "5")int size){
        PageInfo<Orders> list = ordersService.selectOrdersListbyUserId(page,size,userId);
        return YuancityView.success("查询成功").put("data", list);

    }
}

