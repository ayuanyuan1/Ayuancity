package com.yuan.ayuancity.controller;


import com.github.pagehelper.PageInfo;
import com.yuan.ayuancity.entity.Admins;
import com.yuan.ayuancity.entity.Carts;
import com.yuan.ayuancity.entity.Users;
import com.yuan.ayuancity.mapper.CategorysMapper;
import com.yuan.ayuancity.service.CartsService;
import com.yuan.view.YuancityView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author asuyuan
 * @since 2023-04-25
 */
@RestController
@RequestMapping("/ayuancity/carts")
public class CartsController {

    @Autowired
    private CartsService cartsService;


    //根据用户id查询购物列表
    @GetMapping("/list/{userId}")
    public YuancityView list( @PathVariable Integer userId,
                             @RequestParam(value ="pageNum",required = false,defaultValue = "1")int page,
                             @RequestParam(value="pageSize",required = false,defaultValue = "5")int size)
    {
        PageInfo<Carts> list = cartsService.selectCartsListbyUserId(page,size,userId);
        return YuancityView.success("查询成功").put("data", list);
    }

    //添加购物车
    @GetMapping("/addCart/{userId}/{infosId}")
    public YuancityView addCart(@PathVariable Integer userId, @PathVariable Integer infosId )
    {
       int i= cartsService.addCart(userId,infosId);
        return YuancityView.success("添加成功");
    }

    //删除购物车
    @GetMapping("/deleteCart/{cartId}")
    public YuancityView deleteCart(@PathVariable Integer cartId)
    {
        int i= cartsService.deleteCart(cartId);
        return YuancityView.success("删除成功");
    }
}

