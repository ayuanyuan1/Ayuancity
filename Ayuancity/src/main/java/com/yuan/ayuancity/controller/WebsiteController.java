package com.yuan.ayuancity.controller;


import com.github.pagehelper.PageInfo;
import com.yuan.ayuancity.entity.Website;
import com.yuan.ayuancity.service.WebsiteService;
import com.yuan.view.YuancityView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author asuyuan
 * @since 2023-02-07
 */
@RestController
@RequestMapping("/ayuancity/website")
@CrossOrigin
public class WebsiteController {
    @Autowired
    private WebsiteService websiteService;

    /**
     * 查询网站简介列表
     */
    @GetMapping("/list")
    public YuancityView list(Website website,
                             @RequestParam(value ="pageNum",required = false,defaultValue = "1")int page,
                             @RequestParam(value="pageSize",required = false,defaultValue = "5")int size)
    {

        PageInfo<Website> list = websiteService.selectWebsiteList(website,page,size);
        return YuancityView.success("查询成功").put("data",list);
    }


    /**
     * 获取网站简介详细信息
     */
    @GetMapping(value = "/{introduceId}")
    public YuancityView getInfo(@PathVariable("introduceId") Long introduceId)
    {
        Website website = websiteService.selectWebsiteByIntroduceId(introduceId);
        return YuancityView.success("查询成功").put("data", website);
    }

    /**
     * 新增网站简介
     */
    @PostMapping
    public YuancityView add(@RequestBody Website website)
    {websiteService.insertWebsite(website);
        return  YuancityView.success("新增成功");
    }

    /**
     * 修改网站简介
     */
    @PutMapping
    public YuancityView edit(@RequestBody Website website)
    {websiteService.updateWebsite(website);
        return YuancityView.success("修改成功");
    }

    /**
     * 删除网站简介
     */
    @DeleteMapping("/{introduceIds}")
    public YuancityView remove(@PathVariable Long[] introduceIds)
    {websiteService.deleteWebsiteByIntroduceIds(introduceIds);
        return YuancityView.success("删除成功");
    }
}

