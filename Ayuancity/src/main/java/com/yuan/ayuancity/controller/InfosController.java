package com.yuan.ayuancity.controller;


import com.github.pagehelper.PageInfo;
import com.yuan.ayuancity.entity.Infos;
import com.yuan.ayuancity.entity.TypeAndinfo;
import com.yuan.ayuancity.entity.Users;
import com.yuan.ayuancity.entity.dto.InfosDto;
import com.yuan.ayuancity.mapper.InfosMapper;
import com.yuan.ayuancity.mapper.UsersMapper;
import com.yuan.ayuancity.service.InfosService;
import com.yuan.ayuancity.service.TypeAndinfoService;
import com.yuan.view.YuancityView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author asuyuan
 * @since 2023-02-07
 */
@RestController
@RequestMapping("/ayuancity/infos")
@CrossOrigin
public class InfosController {
    @Autowired
    private InfosService infosService;
    @Autowired
    private TypeAndinfoService typeAndinfoService;
    @Resource
    private InfosMapper infosMapper;
    @Resource
    private UsersMapper usersMapper;
    /**
     * 查询供求信息列表+种类+类型
     */
    @GetMapping("/list")
    public YuancityView list(Infos infos,
                             @RequestParam(value ="pageNum",required = false,defaultValue = "1")int page,
                             @RequestParam(value="pageSize",required = false,defaultValue = "5")int size)
    {
        PageInfo<Infos> list = infosService.selectInfosList(infos,page,size);
        return YuancityView.success("查询成功").put("data", list);
    }

    /**
     * 获取供求信息详细信息
     */
    @GetMapping(value = "/{infoId}")
    public YuancityView getInfo(@PathVariable("infoId") Long infoId)
    {
        Infos infos = infosService.selectInfosByInfoId(infoId);
        return YuancityView.success("查询成功").put("data",infos);
    }

    /**
     * 新增供求信息
     */
    @PostMapping
    public YuancityView add(@RequestBody Infos infos)
    {
        infos.setInfoState(0);
        infos.setInfoCreatetime(new Date());
        //1.新增信息,mybatis-plus新增后会把id返回实体类infos
          infosMapper.insert(infos);
        //2.信息和类型连接表
        TypeAndinfo typeAndinfo=new TypeAndinfo();
        typeAndinfo.setInfoId(infos.getInfoId());
        if (infos.getTypeName()!=null){
        //前端返回来的是id
        typeAndinfo.setTypeId(infos.getTypeId());
        typeAndinfoService.insertTypeAndinfo(typeAndinfo);
        }
        return YuancityView.success("新增成功");
    }

    /**
     * 修改供求信息
     */
    @PutMapping
    public YuancityView edit(@RequestBody Infos infos)
    {
 

        //1.修改信息
        infosService.updateInfos(infos);
        //2.修改连接表
        TypeAndinfo typeAndinfo=new TypeAndinfo();
        typeAndinfo.setInfoId(infos.getInfoId());
        typeAndinfo.setTypeId(infos.getTypeId());
        if (infos.getTypeInfoId()!=null){//连接表存在则修改
            typeAndinfo.setTypeInfoId(Integer.parseInt(infos.getTypeInfoId()));
            typeAndinfoService.updateTypeAndinfo(typeAndinfo);
        }else{  //如果没有添加过连接表则创建
            typeAndinfoService.insertTypeAndinfo(typeAndinfo);
        }
        return YuancityView.success("修改成功");
    }

    /**
     * 删除供求信息
     */
    @DeleteMapping("/{infoIds}")
    public YuancityView remove(@PathVariable Long[] infoIds)
    {infosService.deleteInfosByInfoIds(infoIds);
        return YuancityView.success("删除成功");
    }

    /**
     *获取审核信息
     */
    @GetMapping("/verifyInfos")
    public YuancityView verifyInfos(Infos infos,
                               @RequestParam(value ="pageNum",required = false,defaultValue = "1")int page,
                               @RequestParam(value="pageSize",required = false,defaultValue = "5")int size)
    {
        //状态：0审核中，1不合格，2发布中，3已完成
        PageInfo<InfosDto> list= infosService.verify(infos,page,size);
        return YuancityView.success("查询成功").put("data", list);
    }
    /**
     * 修改状态
     */
     @PutMapping("/updateInfotate")
    public YuancityView updateInfotate(@RequestBody Infos infos){
         Infos info=new Infos();
         info.setInfoId(infos.getInfoId());
         info.setInfoState(infos.getInfoState());
         infosService.updateInfos(infos);
         return YuancityView.success("修改成功");
     }


    /*
     前端获取热门推荐
   */
    @GetMapping("/getHotInfos")
    public YuancityView getHotInfos()
    {
        Infos infos=new Infos();
        infos.setCategoryName("求职招聘");
        List<Infos> list1 = infosService.getHotInfos(infos);
        infos.setCategoryName("教育培训");
        List<Infos> list2 = infosService.getHotInfos(infos);
        infos.setCategoryName("房屋信息");
        List<Infos> list3 = infosService.getHotInfos(infos);
        infos.setCategoryName("二手市场");
        List<Infos> list4 = infosService.getHotInfos(infos);
        List<Infos> list=new ArrayList<>();
        if (list1.size()>0){
            list.add(list1.get(0));
            list.add(list1.get(1));
            list.add(list1.get(2));
            list.add(list1.get(3));
        }
        if (list2.size()>0){
            list.add(list2.get(0));
        }
        if (list3.size()>0){
            list.add(list3.get(0));
        }
        if (list4.size()>0){
            list.add(list4.get(0));
        }
        return YuancityView.success("查询成功").put("data", list);
    }
}


