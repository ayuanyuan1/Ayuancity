package com.yuan.ayuancity.entity.dto;

import com.yuan.ayuancity.entity.Infos;
import com.yuan.ayuancity.entity.Users;
import lombok.Data;
import lombok.ToString;

/**
 * <p>
 *     信息表Dto
 * </p>
 *
 * @author asuyuan
 * @since 2023/3/1
 */
@Data
@ToString
public class InfosDto extends Infos {
    //创建信息的用户
    private Users users;
}
