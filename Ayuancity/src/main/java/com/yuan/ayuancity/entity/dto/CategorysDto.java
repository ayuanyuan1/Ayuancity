package com.yuan.ayuancity.entity.dto;

import com.yuan.ayuancity.entity.Categorys;
import com.yuan.ayuancity.entity.Types;
import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * <p>
 *       类型信息dto
 * </p>
 *
 * @author asuyuan
 * @since 2023/2/26
 */
@Data
@ToString
public class CategorysDto extends Categorys {
    //种类集合
    private List<Types> types;
}
