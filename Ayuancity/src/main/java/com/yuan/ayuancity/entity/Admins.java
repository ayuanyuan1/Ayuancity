package com.yuan.ayuancity.entity;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.baomidou.mybatisplus.annotation.*;

import java.util.Date;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author asuyuan
 * @since 2023-02-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tb_admins")
@ApiModel(value="Admins对象", description="管理员表")
@ContentRowHeight(18)//内容行高
@HeadRowHeight(25)//标题行高
@ColumnWidth(20)//列宽，可设置成员变量上
public class Admins implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户id")
    @TableId(value = "admin_id", type = IdType.AUTO)
    @ExcelProperty(value = "用户id",index = 0)
    private Integer adminId;

    @ApiModelProperty(value = "用户姓名")
    @ExcelProperty(value = "用户姓名",index = 1)
    private String adminName;

    @ApiModelProperty(value = "用户账户")
    @ExcelProperty(value = "用户账户",index = 2)
    private String adminAccount;

    @ApiModelProperty(value = "用户密码")
    //导出忽略这个字段
    @ExcelIgnore
    private String adminPassword;

    @ApiModelProperty(value = "用户性别：0男，1女")
    @ExcelProperty(value = "用户性别",index = 3)
    private Integer adminSex;

    @ApiModelProperty(value = "用户头像")
    @ExcelProperty(value = "用户头像",index = 4)
    private String adminImg;

    @ApiModelProperty(value = "用户电话")
    @ExcelProperty(value = "用户电话",index = 5)
    private String adminPhone;

    @ApiModelProperty(value = "用户邮箱")
    @ExcelProperty(value = "用户邮箱",index = 6)
    private String adminEmail;

    @ApiModelProperty(value = "用户登录ip")
    @ExcelProperty(value = "用户登录ip",index = 7)
    private String adminLastip;

    @ApiModelProperty(value = "用户创建时间")
    @TableField(fill = FieldFill.INSERT)//value = ("create_time"),
    @ExcelProperty(value = "用户创建时间",index = 8)
    private Date createTime;

    @ApiModelProperty(value = "用户修改时间")
    @ExcelProperty(value = "用户修改时间",index = 9)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    @ApiModelProperty(value = "删除：0未删除，1删除")
    @TableField(fill = FieldFill.INSERT)
     //导出忽略这个字段
    @ExcelIgnore
    private Integer isDelete;
    @TableField(exist = false)
    private List<Role> roles;
}
