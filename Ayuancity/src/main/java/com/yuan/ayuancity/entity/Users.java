package com.yuan.ayuancity.entity;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.baomidou.mybatisplus.annotation.*;

import java.util.Date;
import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author asuyuan
 * @since 2023-02-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tb_users")
@ApiModel(value="Users对象", description="")
@ContentRowHeight(18)//内容行高
@HeadRowHeight(25)//标题行高
@ColumnWidth(20)//列宽，可设置成员变量上
public class Users implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户id")
    @ExcelProperty(value = "用户id",index = 0)
    @TableId(value = "user_id", type = IdType.AUTO)
    private Integer userId;

    @ApiModelProperty(value = "用户姓名")
    @ExcelProperty(value = "用户姓名",index = 1)
    private String userName;

    @ApiModelProperty(value = "用户账户")
    @ExcelProperty(value = "用户账户",index = 2)
    private String userAccount;

    @ApiModelProperty(value = "用户密码")
    @ExcelIgnore
    private String userPassword;

    @ApiModelProperty(value = "用户性别：0女，1男")
    @ExcelProperty(value = "用户性别",index = 3)
    private Integer userSex;

    @ApiModelProperty(value = "用户头像")
    @ExcelProperty(value = "用户头像",index = 4)
    private String userImg;

    @ApiModelProperty(value = "用户电话")
    @ExcelProperty(value = "用户电话",index = 5)
    private String userPhone;

    @ApiModelProperty(value = "用户邮箱")
    @ExcelProperty(value = "用户邮箱",index = 6)
    private String userEmail;

    @ApiModelProperty(value = "用户登录ip")
    @ExcelProperty(value = "用户登录ip",index = 7)
    private String userLastip;

    @ApiModelProperty(value = "用户创建时间")
    @ExcelProperty(value = "用户创建时间",index = 8)
    @TableField(fill = FieldFill.INSERT)//value = ("create_time"),
    private Date createTime;

    @ApiModelProperty(value = "用户修改时间")
    @ExcelProperty(value = "用户修改时间",index = 9)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    @ApiModelProperty(value = "删除：0未删除，1删除")
    @ExcelIgnore
    @TableField(fill = FieldFill.INSERT)
    private Integer isDelete;

     @TableField(exist = false)
    private List<Role> roles;

    private String userAddress;
    private String userCity;
    private String userSign;

}
