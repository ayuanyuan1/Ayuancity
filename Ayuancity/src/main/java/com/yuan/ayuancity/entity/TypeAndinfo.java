package com.yuan.ayuancity.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 信息连接对象 type_andinfo
 * 
 * @author suyuan
 * @date 2023-02-27
 */
@Data
@TableName("type_and_info")
public class TypeAndinfo
{
    private static final long serialVersionUID = 1L;

    /** 信息类型和信息编号 */
    private Integer typeInfoId;

    /** 供求信息类别编号 */
    private Integer typeId;

    /** 供求信息编号 */
    private Integer infoId;


}
