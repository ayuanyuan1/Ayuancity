package com.yuan.ayuancity.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.util.Date;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author asuyuan
 * @since 2023-02-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tb_infos")
@ApiModel(value="Infos对象", description="")
public class Infos implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "供求信息编号")
    @TableId(value = "info_id", type = IdType.AUTO)
    private Integer infoId;

    @ApiModelProperty(value = "供求信息标题")
    private String infoTitle;

    @ApiModelProperty(value = "供求信息内容")
    private String infoContent;

    @ApiModelProperty(value = "供求信息价格")
    private Double infoPrice;

    @ApiModelProperty(value = "地址")
    private String infoAddress;

    @ApiModelProperty(value = "图片")
    private String infoImg;

    @ApiModelProperty(value = "备注")
    private String infoPostscript;

    @ApiModelProperty(value = "状态：1发布中，2完成")
    private Integer infoState;

    @ApiModelProperty(value = "供求信息创建时间")
    @TableField(fill = FieldFill.INSERT)//value = ("create_time"),
    private Date infoCreatetime;

    @ApiModelProperty(value = "供求信息修改时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date infoUpdatetime;

    @ApiModelProperty(value = "删除：0未删除，1删除")
    @TableField(fill = FieldFill.INSERT)
    private Integer infoDelete;
    @ApiModelProperty(value = "创建信息的用户id")
    private Integer userId;

    //mybatis-plus插入会忽略这个字段
    @TableField(exist = false)
   private  String categoryName;
    @TableField(exist = false)
   private String typeName;
    @TableField(exist = false)
    private String typeInfoId;
    @TableField(exist = false)
    private Integer typeId;

}
