package com.yuan.ayuancity.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author asuyuan
 * @since 2023-04-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tb_comment")
@ApiModel(value="Comment对象", description="")
public class Comment implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "评论者名称")
    private String nickname;

    @ApiModelProperty(value = "评论者邮箱")
    private String email;

    @ApiModelProperty(value = "评论内容")
    private String content;

    @ApiModelProperty(value = "评论者头像")
    private String avatar;

    @ApiModelProperty(value = "评论者时间")
    private Date createTime;

    @ApiModelProperty(value = "评论的父id,-1表示一级评论")
    private Long parentCommentId;

    @ApiModelProperty(value = "评论的信息id")
    private Long infoId;


    //回复的评论
    @TableField(exist = false)
    private List<Comment> replyComments;
    //上一级的评论,用于前端回复评论
    @TableField(exist = false)
    private  Comment parentComment;
    //上一级的评论者的名字
    @TableField(exist = false)
    private String parentCommentName;
    //上一级的评论者的头像
    @TableField(exist = false)
    private String parentCommentAvatar;
}
