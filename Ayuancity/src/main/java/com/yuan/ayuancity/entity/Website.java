package com.yuan.ayuancity.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.util.Date;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author asuyuan
 * @since 2023-02-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tb_website")
@ApiModel(value="Website对象", description="")
public class Website implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "网站编号")
    @TableId(value = "introduce_id", type = IdType.AUTO)
    private Integer introduceId;

    @ApiModelProperty(value = "姓名")
    private String introduceName;

    @ApiModelProperty(value = "地址")
    private String introduceAddress;

    @ApiModelProperty(value = "QQ账号")
    private Integer introduceQq;

    @ApiModelProperty(value = "微信账号")
    private String introduceWechat;

    @ApiModelProperty(value = "电话")
    private String introducePhone;

    @ApiModelProperty(value = "内容")
    private String introduceContent;

    @ApiModelProperty(value = "图片")
    private String introduceImg;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)//value = ("create_time"),
    private Date createTime;

    @ApiModelProperty(value = "修改时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    @ApiModelProperty(value = "删除：0未删除，1删除")
    @TableField(fill = FieldFill.INSERT)
    private Integer isDelete;


}
