package com.yuan.ayuancity.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/*
  权限表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Permission {
    private Integer pid;
    private String pname;
}
