package com.yuan.ayuancity.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author asuyuan
 * @since 2023-02-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tb_types")
@ApiModel(value="Types对象", description="")
public class Types implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "供求信息二级种类编号")
    @TableId(value = "type_id", type = IdType.AUTO)
    private Integer typeId;

    @ApiModelProperty(value = "供求信息二级种类名称")
    private String typeName;

    @ApiModelProperty(value = "父类id")
    private Integer categoryId;
    @ApiModelProperty(value = "父类名称")
    private String categoryName;


    @ApiModelProperty(value = "删除：0未删除，1删除")
    @TableField(fill = FieldFill.INSERT)
    private Integer isDelete;


}
