package com.yuan.ayuancity.entity;

import com.sun.tracing.dtrace.ArgsAttributes;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/*
 角色表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Role {

    private Integer rid;
    private String rname;
    private List<Permission> permissions;
}
