package com.yuan.security;

import com.yuan.ayuancity.entity.Admins;
import com.yuan.ayuancity.entity.Permission;
import com.yuan.ayuancity.entity.Role;
import com.yuan.ayuancity.entity.Users;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * <p>
 *     UserDetails定义了用户信息，
 *     我们可以实现这个接口，然后把UserDetailsServiceimpl查询的用户封装到UserDetails
 *      也可以使用Security中的org.springframework.security.core.userdetails.User的User
 * </p>
 *
 * @author asuyuan
 * @since 2023/3/3
 */
public class SecurityUser implements UserDetails {

    private Users users;
    private Admins admins;
    private String acountname;//登录账号
    private String acountpwd;//登录密码
    private Boolean accountNonLocked;//账号是否可用
    private Boolean userAccountNonExpired; // 账号是否未过期


    public SecurityUser(Users users){
        this.users=users;
        this.acountname=users.getUserAccount();
        this.acountpwd=users.getUserPassword();
    }
    public SecurityUser(Admins admins){
        this.admins=admins;
        this.acountname=admins.getAdminAccount();
        this.acountpwd=admins.getAdminPassword();
    }
    public Users getUsers() {
        return users;
    }
    public Admins getAdmins() {
        return admins;
    }

    /**
    *  TODO:权限还没有开始做
    */
    //权限集合，默认需要加ROLE_前缀
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorities = new ArrayList<>();
        // List<Role> roles=new ArrayList<>();
        // List<Permission> perlist=new ArrayList<>();
        // perlist.add(new Permission(1,"p1"));
        // Role temprole=new Role(1,"admin",perlist);
        // roles.add(temprole);
        // admins.setRoles(roles);
        // for (Role role : admins.getRoles()) {
        //     authorities.add(new SimpleGrantedAuthority("ROLE_" + role.getRname()));
        //     for (Permission permission : role.getPermissions()) {
        //         authorities.add(new SimpleGrantedAuthority("PERMISSION_" + permission.getPname()));
        //     }
        // }
        // authorities.add(new SimpleGrantedAuthority("ROLE_" +"admin"));
        // authorities.add(new SimpleGrantedAuthority("PERMISSION_" + "admin"));
        return null;
    }

    // 密码
    // 用户的加密后的密码， 不加密会使用{noop}前缀
    @Override
    public String getPassword() {
        return  this.acountpwd;
    }
    // 用户名，账号
    @Override
    public String getUsername() {
        return this.acountname;
    }

    // 帐户是否过期
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }
    // 帐户是否锁定
    @Override
    public boolean isAccountNonLocked() {
        //可以在数据库设置account_non_locked ,admins.getAccountNonLocked()==1?true:false
        return true;
    }
    // 凭证是否过期
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }
    // 用户是否可用
    @Override
    public boolean isEnabled() {
        return true;
    }

}
