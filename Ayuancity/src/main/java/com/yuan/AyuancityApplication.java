package com.yuan;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.yuan.ayuancity.mapper")
public class AyuancityApplication {

    public static void main(String[] args) {
        SpringApplication.run(AyuancityApplication.class, args);
    }

}
