package com.yuan.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * <p>
 *  mybatis-plus自动填充
 * </p>
 *
 * @author asuyuan
 * @since 2023/2/8
 */

@Component
public class MyMetaObjectHandler implements MetaObjectHandler {
    @Override
    public void insertFill(MetaObject metaObject) {
        // 是entity属性，不是数据库表字段
       this.setFieldValByName("createTime",new Date(),metaObject);
       this.setFieldValByName("isDelete",0,metaObject);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        this.setFieldValByName("updateTime",new Date(),metaObject);
    }
}
