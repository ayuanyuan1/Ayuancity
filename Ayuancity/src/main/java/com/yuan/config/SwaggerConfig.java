package com.yuan.config;

import com.google.common.base.Predicates;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * <p>
 * SwaggerConfig配置类
 * </p>
 *
 * @author asuyuan
 * @since 2023/2/8
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    //http://localhost:8080/swagger-ui.html
    @Bean
    public Docket webApiInfoConfig(){
       return new Docket(DocumentationType.SWAGGER_2)
               .groupName("SuyuanWebApi")
               .apiInfo(webApiInfo())
               .select()
               .paths(Predicates.not(PathSelectors.regex("/admin/.*")))
               .paths(Predicates.not(PathSelectors.regex("/error.*")))
               .build();
    }


    private ApiInfo webApiInfo(){
      return  new ApiInfoBuilder()
              .title("网站,源同城信息供求API文档")
              .description("本文档主要用于源同城网站接口测试")
              .version("2.0")
              .contact(new Contact("Springboot", "1796644863@qq.com", "1796644863@qq.com"))
              .build();
    }


}
