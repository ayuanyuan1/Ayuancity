package com.yuan.config;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * <p>
 *       Mybatis配置类
 * </p>
 *
 * @author asuyuan
 * @since 2023/2/15
 */
@Configuration
@EnableTransactionManagement
public class MybatisPlusConfig {

    /**
     * 分页插件
     */
    // @Bean
    public PaginationInterceptor PaginationInterceptor(){
        return new PaginationInterceptor();
    }

}
