package com.yuan.view;

import org.springframework.security.crypto.bcrypt.BCrypt;

import java.util.Objects;

/**
 * <p>
 *      密码校验
 * </p>
 *
 * @author asuyuan
 * @since 2023/3/5
 */
public class BcryptHelper {


    private BcryptHelper() {
    }

    public static String bcrypt(String password) {
        Objects.requireNonNull(password, "password must not be null.");
        return BCrypt.hashpw(password, BCrypt.gensalt());
    }

    public static boolean matches(String password, String encrypt) {
        // return new BCryptPasswordEncoder().matches(password, encrypt);
        return BCrypt.checkpw(password, encrypt);
    }
}
