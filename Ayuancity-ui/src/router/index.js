import Vue from 'vue'
import Router from 'vue-router'

// in development-env not use lazy-loading, because lazy-loading too many pages will cause webpack hot update too slow. so only in production use lazy-loading;
// detail: https://panjiachen.github.io/vue-element-admin-site/#/lazy-loading

Vue.use(Router)

/* Layout */
import Layout from '../views/layout/Layout'

/**
* hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
* alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
*                                if not set alwaysShow, only more than one route under the children
*                                it will becomes nested mode, otherwise not show the root menu
* redirect: noredirect           if `redirect:noredirect` will no redirect in the breadcrumb
* name:'router-name'             the name is used by <keep-alive> (must set!!!)
* meta : {
    title: 'title'               the name show in submenu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar,
  }
**/
export const constantRouterMap = [
  { path: '/login', component: () => import('@/views/login/index'), hidden: true },
  { path: '/404', component: () => import('@/views/404'), hidden: true },
  { path: '/index', redirect: '/' },
  {
    path: '/',
    component: Layout,
    children: [
      {
        path: '',
        name: '首页',
        component: () => import('@/views/index'),
        meta: { title: '首页', icon: 'form' }
      }
    ]
  },
  {
    path: '/people',
    component: Layout,
    name: '人员管理',
    meta: { title: '人员管理', icon: 'example' },
    children:[
      {
        path: 'user',
        name: '用户管理',
        component: ()=> import('@/views/people/user'),
        meta:{title:'用户管理',icon: 'form'}
      },
      {
        path: 'admin',
        name: '管理员管理',
        component: ()=>import('@/views/people/admin'),
        meta:{title:'管理员管理',icon:'form'}
      }

    ]

  },
  {
    path: '/system',
    component: Layout,
    name: '信息管理',
    meta: { title: '信息管理', icon: 'example' },
    children:[
      {
        path: 'category',
        name: '信息类型管理',
        component: ()=> import('@/views/system/category'),
        meta:{title:'信息类型管理',icon: 'form'}
      },
      {
        path: 'type',
        name: '信息种类管理',
        component: ()=>import('@/views/system/type'),
        meta:{title:'信息种类管理',icon:'form'}
      },
      {
        path: 'info',
        name: '信息管理',
        component: ()=>import('@/views/system/info'),
        meta:{title:'信息管理',icon:'form'}
      }
      // ,{
      //   path: 'infotype',
      //   name: '分类管理',
      //   component: ()=>import('@/views/system/infotype'),
      //   meta:{title:'分类管理',icon:'form'}
      // }

    ]

  },
  {
    path: '/verify',
    component: Layout,
    name:'审核中心',
    meta: { title: '审核中心', icon: 'example' },
    children:[
     {
       path:'',
       name:'信息',
       component:()=>import('@/views/verify/index'),
       meta:{title:'审核信息',icon:'form'}
     },
     {
       path: 'info',
       name: '信息管理',
       component: ()=>import('@/views/verify/failure'),
       meta:{title:'发布失败',icon:'form'}
     } ,
     {
       path: 'type',
       name: '信息通过',
       component: ()=>import('@/views/verify/success'),
       meta:{title:'发布通过',icon:'form'}
     }
     ,
     {
       path: 'finish',
       name: '信息完成',
       component: ()=>import('@/views/verify/finish'),
       meta:{title:'信息完成',icon:'form'}
     }
    ]
 },
  {
    path: '/profile',
    component: Layout,
    name:'个人中心',
    children:[
     {
       path:'',
       name:'个人中心',
       component:()=>import('@/views/profile/index'),
       meta:{title:'个人中心',icon:'form'}
     }
    ]
 },
    {
       path: '/web',
       component: Layout,
       name:'网站管理',
       children:[
        {
          path:'',
          name:'网站管理',
          component:()=>import('@/views/web/index'),
          meta:{title:'网站管理',icon:'form'}
        }
       ]
    },
    {
      path: '/后台管理',
      component: Layout,
      name:'后台管理',
      children:[
       {
         path:'',
         name:'后台管理',
         component:()=>import('@/views/backstage/index'),
         meta:{title:'后台管理',icon:'form'}
       }
      ]
   },

  { path: '*', redirect: '/404', hidden: true }
]

export default new Router({
  // mode: 'history', //后端支持可开
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRouterMap
})
