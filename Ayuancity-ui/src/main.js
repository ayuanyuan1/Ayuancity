import Vue from 'vue'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/en' // lang i18n

import '@/styles/index.scss' // global css
import '@/assets/styles/index.scss' // global css
import '@/assets/styles/sy.scss' // sy css

import App from './App'
import router from './router'
import store from './store'

import '@/icons' // icon
import '@/permission' // permission control

// import iView from 'iview' // 导入组件库
// import 'iview/dist/styles/iview.css';// 导入样式
// 图片预览组件
import ImagePreview from "@/components/ImagePreview"
// import Pagination from "@/components/Pagination"
//弹框
import plugins from './plugins/index'
Vue.use(ElementUI,{ locale })
// Vue.use(iView);
Vue.use(plugins);

Vue.component('ImagePreview', ImagePreview)

// Vue.component('Pagination', Pagination)

Vue.config.productionTip = false
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})


