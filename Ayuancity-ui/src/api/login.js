import request from '@/utils/request'

export function login(username, password) {
  return request({
    url: '/city/admin/login',
    method: 'post',
    data: {
      username,
      password
    }
  })
}

export function getInfo(token) {
  return request({
    url: '/city/admin/info',
    method: 'get',
    params: { token }
  })
}

export function logout() {
  return request({
    url: '/city/admin/logout',
    method: 'post'
  })
}

//获取验证码
export function getCaptcha(){
    return request({
      url: "/city/v1/getCaptcha?type=houtai",
      method: 'get'
    })
}
