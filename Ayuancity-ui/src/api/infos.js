import request from '@/utils/request'

// 查询供求信息列表
export function listInfos(query) {
  return request({
    url: '/ayuancity/infos/list',
    method: 'get',
    params: query
  })
}

// 查询供求信息详细
export function getInfos(infoId) {
  return request({
    url: '/ayuancity/infos/' + infoId,
    method: 'get'
  })
}

// 新增供求信息
export function addInfos(data) {
  return request({
    url: '/ayuancity/infos',
    method: 'post',
    data: data
  })
}

// 修改供求信息
export function updateInfos(data) {
  return request({
    url: '/ayuancity/infos',
    method: 'put',
    data: data
  })
}

// 删除供求信息
export function delInfos(infoId) {
  return request({
    url: '/ayuancity/infos/' + infoId,
    method: 'delete'
  })
}
// 查询供求信息列表--审核未通过的信息
export function verifyInfos(query) {
  return request({
    url: '/ayuancity/infos/verifyInfos',
    method: 'get',
    params: query
  })
}
//审核信息-修改信息状态
export function updateInfotate(data) {
  return request({
    url: '/ayuancity/infos',
    method: 'put',
    data: data
  })
}