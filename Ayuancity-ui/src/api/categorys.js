import request from '@/utils/request'

// 查询供求信息类型列表
export function listCategorys(query) {
  return request({
    url: '/ayuancity/categorys/list',
    method: 'get',
    params: query
  })
}
export function listCategorysAll() {
  return request({
    url: '/ayuancity/categorys/listAll',
    method: 'get',
  })
}

// 查询供求信息类型详细
export function getCategorys(categoryId) {
  return request({
    url: '/ayuancity/categorys/' + categoryId,
    method: 'get'
  })
}

// 新增供求信息类型
export function addCategorys(data) {
  return request({
    url: '/ayuancity/categorys',
    method: 'post',
    data: data
  })
}

// 修改供求信息类型
export function updateCategorys(data) {
  return request({
    url: '/ayuancity/categorys',
    method: 'put',
    data: data
  })
}

// 删除供求信息类型
export function delCategorys(categoryId) {
  return request({
    url: '/ayuancity/categorys/' + categoryId,
    method: 'delete'
  })
}
