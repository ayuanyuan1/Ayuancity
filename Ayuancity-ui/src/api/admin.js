import request from '@/utils/request'

// 查询管理员列表
export function listAdmins(query) {
  return request({
    url: '/ayuancity/admins/list',
    method: 'get',
    params: query
  })
}

// 查询管理员详细
export function getAdmins(adminId) {
  return request({
    url: '/ayuancity/admins/' + adminId,
    method: 'get'
  })
}

// 新增管理员
export function addAdmins(data) {
  return request({
    url: '/ayuancity/admins',
    method: 'post',
    data: data
  })
}

// 修改管理员
export function updateAdmins(data) {
  return request({
    url: '/ayuancity/admins',
    method: 'put',
    data: data
  })
}

// 删除管理员
export function delAdmins(adminId) {
  return request({
    url: '/ayuancity/admins/' + adminId,
    method: 'delete'
  })
}

// 修改密码
export function editAdminPwd(adminId, oldpwd,newpwd) {
  return request({
    url: '/ayuancity/admins/editAdminPwd',
    method: 'post',
    params:{
      adminId,
      oldpwd,
      newpwd
    }
  })
}