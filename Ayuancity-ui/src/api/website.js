import request from '@/utils/request'

// 查询网站简介列表
export function listWebsite(query) {
  return request({
    url: '/ayuancity/website/list',
    method: 'get',
    params: query
  })
}

// 查询网站简介详细
export function getWebsite(introduceId) {
  return request({
    url: '/ayuancity/website/' + introduceId,
    method: 'get'
  })
}

// 新增网站简介
export function addWebsite(data) {
  return request({
    url: '/ayuancity/website',
    method: 'post',
    data: data
  })
}

// 修改网站简介
export function updateWebsite(data) {
  return request({
    url: '/ayuancity/website',
    method: 'put',
    data: data
  })
}

// 删除网站简介
export function delWebsite(introduceId) {
  return request({
    url: '/ayuancity/website/' + introduceId,
    method: 'delete'
  })
}
