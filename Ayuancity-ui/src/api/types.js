import request from '@/utils/request'

// 查询供求信息二级种类列表-分页
export function listTypes(query) {
  return request({
    url: '/ayuancity/types/list',
    method: 'get',
    params: query
  })
}

// 查询供求信息二级种类列表-全部
export function listTypeAlls(query) {
  return request({
    url: '/ayuancity/types/listAll',
    method: 'get',
    params: query
  })
}

// 查询供求信息二级种类详细
export function getTypes(typeId) {
  return request({
    url: '/ayuancity/types/' + typeId,
    method: 'get'
  })
}

// 新增供求信息二级种类
export function addTypes(data) {
  return request({
    url: '/ayuancity/types',
    method: 'post',
    data: data
  })
}

// 修改供求信息二级种类
export function updateTypes(data) {
  return request({
    url: '/ayuancity/types',
    method: 'put',
    data: data
  })
}

// 删除供求信息二级种类
export function delTypes(typeId) {
  return request({
    url: '/ayuancity/types/' + typeId,
    method: 'delete'
  })
}


