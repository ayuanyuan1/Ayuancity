# vue-admin-template

> A minimal vue admin template with Element UI & axios & iconfont & permission control & lint

**Live demo:** http://panjiachen.github.io/vue-admin-template

[中文文档](https://github.com/PanJiaChen/vue-admin-template/blob/master/README-zh.md)

## Build Setup

## 新加组件
1.添加弹窗iview插件
官网：https://blog.csdn.net/weixin_30257433/article/details/97024532
安装：npm install iview --save
2.安装图片裁剪依赖 "vue-cropper": "0.5.5",
解决办法： npm i --save  vue-cropper@0.5.5

## 遇到的BUG
1.后端接口正确，登录无反应
解决办法：store/user.js修改返回类型
2.男女选择下拉框，接受到后端的key,没有对应value
解决办法：key要和实体类的类型一样，我的是int
3.el-image不显示 [ "element-ui": "2.4.6"]
解决办法：element-ui2.8.2才引入的组件 
1).卸载 npm uninstall element-ui --save
2).安装 npm i element-ui -S
